const axios = require('axios')
const sendmail = require('sendmail')()

exports.handler = (event, context, callback) => {
    const checkUnavailableVideos = async() => {
        try {
            const unavailableVideosUrl = process.env.DELETEDVIDEOS_URL
            const unavailableVideosResponse = await axios.get(unavailableVideosUrl, {
                params: {
                    channelid: event.query.channelid
                }
            })
            const unavailableVideos = unavailableVideosResponse.data
            
            if (unavailableVideos.length > 0) {
                const promises = unavailableVideos.map(async (video) => {
                    const response = await axios.post(unavailableVideosUrl, video)
                    
                    return response.data
                })
                
                const unavailableVideosData = await Promise.all(promises)
                
                unavailableVideosData.map(video => {
                    const emailData = prepareEmail(video)
                    
                    sendMail(emailData)
                })
                
                callback(null, 'success')  
            }
        } catch(error) {
            callback(error, null)
        }
    }
    
    const prepareEmail = (video) => {
        const subject = `You have an unavailable video in one of your Youtube playlists !`
        const message = `The following video is unavailable:<br>
        ID: ${video.id}<br>
        Title: ${video.title}<br>
        Playlist ID: ${video.playlistId}<br>
        Position: ${video.position}<br>
        <br>
        Direct link to playlist: https://www.youtube.com/playlist?list=${video.playlistId}
        `
        
        return {
            subject,
            message
        }
    }
    
    const sendMail = (data) => {
        sendmail({
            from: process.env.FROM,
            to: process.env.RECIPIENT_EMAIL_ADDRESS,
            subject: data.subject,
            html: data.message
        }, 
        (err, reply) => {
            console.log(err && err.stack)
            console.dir(reply)
        })
    }

    checkUnavailableVideos()
}